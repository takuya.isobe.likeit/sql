﻿use textbook_sample;

SELECT
    m1.*
FROM
    m_customer m
INNER JOIN
    t_sale t
ON
    m.cust_id = t.cust_id
INNER JOIN
    t_sale_detail t1
ON
    t.sale_id = t1.sale_id
INNER JOIN
    m_item m1
ON
    t1.item_id = m1.item_id
WHERE
   m.cust_nm = '鈴木'
AND
    t.sale_date = '2019-05-05'
