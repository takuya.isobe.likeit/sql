﻿USE practice ;

SELECT category_name, SUM(b.item_price) AS total_price FROM item_category a 
INNER JOIN item b ON a.category_id = b.category_id GROUP BY category_name
ORDER BY total_price DESC ;

